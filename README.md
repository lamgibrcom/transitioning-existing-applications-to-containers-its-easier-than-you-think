# Transitioning Existing Applications to Containers Its Easier Than You Think
**Author:** Eric Getchell, Red Hat Inc., Eric Deandera, Red Hat Inc.

**Level:** Intermediate

**Technologies:** Red Hat OpenShift, Red Hat CodeReady Workspaces, Tomcat, Jenkins, Spring Boot, Containers

**Summary:** Step through a simple workflow that demonstrates portability between non-containerized and containerized applications. Learn how to leverage Source 2 Image (S2I) for containerizing existing applications and add Jenkins into your application build and test lifecycles.

**Target Products:** Red Hat OpenShift, Red Hat CodeReady Workspaces, Jenkins, Red Hat JWS, Spring Boot

**Source:** https://gitlab.com/redhatsummitlabs/transitioning-existing-applications-to-containers-its-easier-than-you-think

## What is it?
This lab will demonstrate typical developer workflow for working with Java web applications in a both a traditional and containerized format.  The labs start with importing of a simple Java web application into Code Ready Workspaces, deploying to a local Tomcat instance, and then showing how this same web application can be seamlessly containerized and deployed into OpenShift, Red Hat's container management platform. We will then integrate the application build and deployment into a containerized version of Jenkins to demonstrate integration of the build and test lifecycle of your application within OpenShift.  Finally we will demonstrate containerizing a Spring Boot application.

## System requirements
You will need a web browser and access to an instance of OpenShift.

## Labs
* [lab1 - Introduction to CodeReady Workspaces](/labs/lab1.md)
* [lab2 - Containerizing a Tomcat Application](/labs/lab2.md)
* [lab3 - Interacting with OpenShift from CodeReady Workspaces](/labs/lab3.md)
* [lab4 - Adding Jenkins](/labs/lab4.md)
* [lab5 - Spring Boot](/labs/lab5.md)
